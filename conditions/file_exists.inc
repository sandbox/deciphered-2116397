<?php
/**
 * "File exists" Rules condition.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_condition_file_exists_info() {
  return array(
    'label' => t('File exists'),
    'group' => t('File Rules'),
    'base' => 'file_rules_condition_file_exists',
    'parameter' => array(
      'path' => array(
        'label' => t('Path'),
        'type' => 'text',
      ),
    ),
  );
}

/**
 * Callback for 'file_exists' rules condition.
 */
function file_rules_condition_file_exists($path) {
  return file_exists($path);
}
