<?php
/**
 * @file
 * "Put contents" Rules action.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_action_file_put_contents_info() {
  return array(
    'label'     => t('Put contents'),
    'group'     => t('File Rules'),
    'base'      => 'file_rules_rules_file_put_contents',
    'arguments' => array(
      'filename' => array(
        'type'  => 'text',
        'label' => t('Filename'),
      ),
      'data'     => array(
        'type'  => 'text',
        'label' => t('Data'),
      ),
    ),
  );
}

/**
 * Callback for 'file_put_contents' rules action.
 */
function file_rules_rules_file_put_contents($filename, $data) {
  file_put_contents($filename, $data);
}
