<?php
/**
 * @file
 * "Copy files" Rules action.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_action_file_unmanaged_copy_info() {
  return array(
    'label' => t('Copy files'),
    'group' => t('File Rules'),
    'base' => 'file_rules_action_file_unmanaged_copy',
    'arguments' => array(
      'source' => array(
        'type' => 'text',
        'label' => t('Source'),
        'description' => '',
      ),
      'destination' => array(
        'type' => 'text',
        'label' => t('Destination'),
        'description' => '',
      ),
      'replace' => array(
        'type' => 'decimal',
        'label' => t('Replace behaviour'),
        'options list' => 'file_rules_file_exists_options',
      ),
    ),
  );
}

/**
 * Callback for 'file_unmanaged_copy' rules action.
 */
function file_rules_action_file_unmanaged_copy($source, $destination, $replace) {
  $source = drupal_realpath($source);
  switch (TRUE) {
    case is_dir($source):
      foreach (file_rules_file_scan_directory($source, '/\.*/') as $file) {
        $file_destination = str_replace($source, $destination, $file->uri);
        file_rules_action_file_unmanaged_copy($file->uri, $file_destination, $replace);
      }
      break;

    case is_file($source):
      $pathinfo = pathinfo($destination);
      if (file_prepare_directory($pathinfo['dirname'], FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        file_unmanaged_copy($source, $destination, $replace);
      }
      break;
  }
}
