<?php
/**
 * @file
 * "Prepare directory" Rules action.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_action_file_prepare_directory_info() {
  return array(
    'label' => t('Prepare directory'),
    'group' => t('File Rules'),
    'base' => 'file_rules_rules_file_prepare_directory',
    'arguments' => array(
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'description' => t('The directory to prepare.'),
      ),
      'create_directory' => array(
        'type' => 'boolean',
        'label' => t('Create directory'),
      ),
      'modify_permissions' => array(
        'type' => 'boolean',
        'label' => t('Modify permissions'),
      ),
    ),
  );
}

/**
 * Callback for 'file_prepare_directory' rules action.
 */
function file_rules_rules_file_prepare_directory($directory, $create_directory, $modify_permissions) {
  switch (TRUE) {
    case $create_directory && $modify_permissions:
      $options = FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS;
      break;

    case $create_directory:
      $options = FILE_CREATE_DIRECTORY;
      break;

    case $modify_permissions:
      $options = FILE_MODIFY_PERMISSIONS;
      break;
  }

  file_prepare_directory($directory, $options);
}
