<?php
/**
 * @file
 * "Find and replace" Rules action.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_action_find_replace_info() {
  return array(
    'label' => t('Find and replace'),
    'group' => t('File Rules'),
    'base' => 'file_rules_action_find_replace',
    'arguments' => array(
      'path' => array(
        'type' => 'text',
        'label' => t('Target path'),
        'description' => t('The target file/directory path.'),
      ),
      'find' => array(
        'type' => 'text',
        'label' => t('Find'),
        'description' => t('The string to find.'),
      ),
      'replace' => array(
        'type' => 'text',
        'label' => t('Replace'),
        'description' => t('The string to replace.'),
        'optional' => TRUE,
      ),
      'scope' => array(
        'type' => 'decimal',
        'label' => t('Scope'),
        'options list' => 'file_rules_action_find_replace_options',
      ),
    ),
  );
}

/**
 * Callback for 'find_replace' rules action.
 */
function file_rules_action_find_replace($path, $find, $replace = '', $scope) {
  // Find and replace within the file path.
  if ($scope & FILE_RULES_FIND_REPLACE_PATH) {
    switch (TRUE) {
      case is_dir($path):
        foreach (file_rules_file_scan_directory($path, '/\.*/') as $file) {
          file_rules_action_find_replace($file->uri, $find, $replace, FILE_RULES_FIND_REPLACE_PATH);
        }
        break;

      case is_file($path):
        $file_destination = str_replace($find, $replace, $path);
        if ($file_destination != $path) {
          require_once drupal_get_path('module', 'file_rules') . '/actions/file_unmanaged_move.inc';
          file_rules_action_file_unmanaged_move($path, $file_destination, FILE_EXISTS_REPLACE);
        }
        break;
    }
  }

  // Find and repalce with the file contents.
  if ($scope & FILE_RULES_FIND_REPLACE_CONTENTS) {
    switch (TRUE) {
      case is_dir($path):
        foreach (file_rules_file_scan_directory($path, '/\.*/') as $file) {
          file_rules_action_find_replace($file->uri, $find, $replace, FILE_RULES_FIND_REPLACE_CONTENTS);
        }
        break;

      case is_file($path):
        $before = file_get_contents($path);
        $after = str_replace($find, $replace, $before);
        if ($before != $after) {
          file_unmanaged_save_data($after, $path, FILE_EXISTS_REPLACE);
        }
        break;
    }
  }
}
