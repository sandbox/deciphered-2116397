<?php
/**
 * @file
 * "Delete files/directory recursively" Rules action.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_action_file_unmanaged_delete_recursive_info() {
  return array(
    'label' => t('Delete files/directory recursively'),
    'group' => t('File Rules'),
    'base' => 'file_rules_action_file_unmanaged_delete_recursive',
    'arguments' => array(
      'path' => array(
        'type' => 'text',
        'label' => t('Path'),
        'description' => t('The file/directory to delete.'),
      ),
    ),
  );
}

/**
 * Callback for 'file_unmanaged_delete_recursive' rules action.
 */
function file_rules_action_file_unmanaged_delete_recursive($path) {
  file_unmanaged_delete_recursive($path);
}
