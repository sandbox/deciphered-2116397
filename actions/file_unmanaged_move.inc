<?php
/**
 * @file
 * "File Unmanaged Copy" Rules action.
 */

/**
 * Implements file_rules_TYPE_HOOK_info().
 */
function file_rules_action_file_unmanaged_move_info() {
  return array(
    'label' => t('Move files'),
    'group' => t('File Rules'),
    'base' => 'file_rules_action_file_unmanaged_move',
    'arguments' => array(
      'source' => array(
        'type' => 'text',
        'label' => t('Source'),
        'description' => t(''),
      ),
      'destination' => array(
        'type' => 'text',
        'label' => t('Destination'),
        'description' => t(''),
      ),
      'replace' => array(
        'type' => 'decimal',
        'label' => t('Replace behaviour'),
        'options list' => 'file_rules_file_exists_options',
      ),
    ),
  );
}

/**
 * Callback for 'file_unmanaged_move' rules action.
 */
function file_rules_action_file_unmanaged_move($source, $destination, $replace) {
  $source = drupal_realpath($source);
  switch (TRUE) {
    case is_dir($source):
      foreach (file_rules_file_scan_directory($source, '/\.*/') as $file) {
        $file_destination = str_replace($source, $destination, $file->uri);
        file_rules_action_file_unmanaged_move($file->uri, $file_destination, $replace);
      }
      break;

    case is_file($source):
      $pathinfo = pathinfo($destination);
      if (file_prepare_directory($pathinfo['dirname'], FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        file_unmanaged_move($source, $destination, $replace);
      }
      break;
  }
}
