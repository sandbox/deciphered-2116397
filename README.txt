The File Rules module provides File based actions, conditions and events for the
Rules module, from copying files to finding and replacing a string in the
contents of all files inside a specified directory.



Features
--------------------------------------------------------------------------------

- Actions:
  - Copy files: Copy files or directory.
  - Delete files/directory recursively.
  - Find and replace: Find a replace a string in path and/or file contents.
  - Move files: Move files or directory.
  - Prepare directory: Create directory and/or modify permissions.
  - Put contents: Write data to the file system.
- Conditions:
  - File exists: Does file or directory exist?
